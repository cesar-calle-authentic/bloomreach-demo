# Bloomreach Demo

This project has integration for deployo with gitab ci/cd a Bloomreach Project with terrafom in EKS.

## Structure Project

``` bash
    .
    ├── Dockerfile.builder
    ├── README.md
    ├── pom.xml
    └── terraform
       ├── builder
       │   └── gitlab-terraform.sh
       ├── cluster
       │   └── main.tf
       ├── compiler
       │   └── main.tf
       ├── ecr
       │   └── main.tf
       ├── kubectl
       │   └── kubect.tf
       ├── main.tf
       └── rds
          └── main.tf
```

## Custom builder for gitlab

It need a custom docker file with next features:

- Docker
- java 8
- maven 3.6.3
- AWS-Cli
- kubectl
- terraform
- gitlab-terraform

For this, you have to push this image to gitlab registry. Run: 

```bash
    docker build -t builder:latest -f Dockerfile.builder .
    docker login $CI_REGISTRY_IMAGE
    docker tag builder:latest $CI_REGISTRY_IMAGE/builder:latest
    docker push $CI_REGISTRY_IMAGE/builder:latest
```

- $CI_REGISTRY_IMAGE is a base registry path of your project
In step 2 of script you will have put your credentials

# Bloomreach configuration for Docker with Mysql

For build a Bloomreach docker image with mysql you have to add this to pom.xml

```xml
  ...
  </properties>
    ...
    <mysql.connector.version>8.0.25</mysql.connector.version>
  </properties>
  ...
```

## Terraform deployment

For this integration with terraform we have the terraform directory, with sub modules. We need this providers, defined in terraform/main.tf:

- aws
- kubernetes

This script have this sub modules:

- ecr: for create a Elastic Container Registry where will be push the docker images
- compiler: for build blomreach docker image and push it to ecr
- cluster: for create VPC and NodeCluster for kubernetes in aws
- rds: for create RDS image with Mysql
- kubectl: for deployment pods of Bloomreach and connect it to RDS

## Gitlab CI/CD Variables

We need configure this variables

- AWS_DEFAULT_REGION
- AWS_DOCKER_REGISTRY: full path of docker registry that will be create
- TF_VAR_repository_name: only path name in account of registry in aws
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- TF_VAR_aws_access_key
- TF_VAR_aws_secret_key

You can override all variable used ini terraform/main.tf adding as CI/CD Variable with prefix TF_VAR_

- eks_cluster_name
- vpc_name
- rds_name
- mysql_database
- mysql_username
- mysql_password
- app_name
- deployment_name

## Important

The first time that script will be execute, this will be have error. You have to verify if this error is on deployment in kubernetes, its normal, you only have to re run for fix it.

Other common error can be on publish kubernetes service on module.kubectl.kubernetes_service.mysvc, this error is because the time of publish LoadBalancer in aws is a lot. You have to waiting for it will be created.