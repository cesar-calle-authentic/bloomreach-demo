terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "http" {} 
}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
  type    = string
  default = "us-east-1"
}
variable "aws_profile" {
  type    = string
  default = "default"
}
variable "service" {
  type    = string
  default = "bloomreach-demo-eks"
}
variable "eks_cluster_name" {
  type    = string
  default = "BloomreachKube"
}
variable "repository_name" {}

variable "vpc_name" {
  type = string
  default = "BloomreachDemo"
}

variable "rds_name" {
  type = string
  default = "bloomreachdemo"
}

variable "mysql_database" {
  type = string
  default = "mywpdb"
}

variable "mysql_username" {
  type = string
  default = "mohit"
}

variable "mysql_password" {
  type = string
  default = "passmohit"
}

variable "app_name" {
  type = string
  default = "bloomreach-app"
}

variable "deployment_name" {
  type = string
  default = "bloomreach-deployment"
}

provider "aws" {
  profile    = var.aws_profile
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

module "ecr" {
  source          = "./ecr"
  service         = var.service
  repository_name = var.repository_name
}

module "compiler" {
  depends_on     = [module.ecr]
  source          = "./compiler"
}

module "cluster" {
  source           = "./cluster"
  vpc_name         = var.vpc_name
  eks_cluster_name = var.eks_cluster_name
  service          = var.service
  aws_region       = var.aws_region
  aws_profile      = var.aws_profile
}

module "rds" {
  source         = "./rds"
  rds_name       = var.rds_name
  mysql_database = var.mysql_database
  mysql_username = var.mysql_username
  mysql_password = var.mysql_password
  service        = var.service
  depends_on     = [module.cluster]
}

module "kubectl" {
  source          = "./kubectl"
  repository_name = var.repository_name
  app_name        = var.app_name
  deployment_name = var.deployment_name
  rds_name       = var.rds_name
  mysql_database = var.mysql_database
  mysql_username = var.mysql_username
  mysql_password = var.mysql_password
  eks_cluster_name = var.eks_cluster_name
  service         = var.service
  depends_on      = [module.rds, module.compiler]
}