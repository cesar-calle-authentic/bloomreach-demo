variable service {}
variable repository_name {}
variable app_name {}
variable deployment_name {}
variable rds_name {}
variable mysql_database {}
variable mysql_username {}
variable mysql_password {}
variable eks_cluster_name {}

# Fetching VPC or SubnetIDS
data "aws_vpcs" "eks-vpc" {
  tags = {
    service = var.service
  }
}
data "aws_subnet_ids" "subnetids" {
  vpc_id = element(tolist(data.aws_vpcs.eks-vpc.ids), 0)
}

data "aws_ecr_repository" "repository" {
  name = var.repository_name
  tags = {
    service = var.service
  }
}
data  "aws_db_instance" "mydb" {
  db_instance_identifier = var.rds_name
  tags = {
    Name    = var.rds_name
  }
}

# Updating Local config file.
resource "null_resource" "relogin-command" {

  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name ${var.eks_cluster_name}"
  }
}

# Deployment resources
resource "kubernetes_deployment" "mydeployment" {
  depends_on = [null_resource.relogin-command, ]
  metadata {
    name = var.deployment_name
    labels = {
      app = var.app_name
    }
  }
  # Spec for deployment
  spec {
    replicas = 3
    selector {
      match_labels = {
        app = var.app_name
      }
    }
    template {
      metadata {
        labels = {
          app = var.app_name
        }
      }
      # Spec for container
      spec {
        container {
          # Image to be used 
          image = "${data.aws_ecr_repository.repository.repository_url}:latest"
          # Providing host, credentials and database name in environment variable
          env {
            name  = "MYSQL_DB_HOST"
            value = data.aws_db_instance.mydb.address
          }
          env {
            name  = "MYSQL_DB_USER"
            value = var.mysql_username
          }
          env {
            name  = "MYSQL_DB_PASSWORD"
            value = var.mysql_password
          }
          env {
            name  = "MYSQL_DB_NAME"
            value = var.mysql_database
          }

          env {
            name = "profile"
            value = "mysql"
          }

          name = "container"
          port {
            container_port = 8080
          }
        }
      }
    }
  }

}


# Storage class, defining storage provisoner
# resource "kubernetes_storage_class" "kubeSC" {
#   depends_on = [aws_db_instance.mydb, ]
#   metadata {
#     name = "kubesc"
#   }
#   storage_provisioner = "kubernetes.io/aws-ebs"
#   reclaim_policy      = "Retain"
#   parameters = {
#     type = "gp2"
#   }
# }


# PVC 
# resource "kubernetes_persistent_volume_claim" "pvc" {
#   depends_on = [kubernetes_storage_class.kubeSC, ]
#   metadata {
#     name = "bloomreach-pvc"
#     labels = {
#       app = "bloomreach-frontend"
#     }
#   }
#   spec {
#     access_modes       = ["ReadWriteOnce"]
#     storage_class_name = kubernetes_storage_class.kubeSC.metadata.0.name
#     resources {
#       requests = {
#         storage = "2Gi"
#       }
#     }
#   }
# }


# LoadBalancer service 
resource "kubernetes_service" "mysvc" {
  depends_on = [kubernetes_deployment.mydeployment, ]
  metadata {
    name = "${var.app_name}-service"
    labels = {
      app     = var.app_name
      service = var.service
    }
  }
  wait_for_load_balancer = false
  spec {
    selector = {
      app = var.app_name
    }
    port {
      port = 80
      target_port      = 8080
    }
    type = "LoadBalancer"
  }
}


resource "time_sleep" "wait_120_seconds" {
  depends_on = [kubernetes_service.mysvc]

  create_duration = "120s"
}


resource "null_resource" "command" {
  depends_on = [time_sleep.wait_120_seconds, ]

  provisioner "local-exec" {
    command = "echo  ${kubernetes_service.mysvc.id}"
  }
}