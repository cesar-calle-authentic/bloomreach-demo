variable service {}
variable repository_name {}

resource "aws_ecr_repository" "repository" {
  name = var.repository_name
  tags = {
    service = var.service
  }
}