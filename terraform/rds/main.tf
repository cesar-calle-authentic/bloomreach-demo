variable service {}
variable rds_name {}
variable mysql_database {}
variable mysql_username {}
variable mysql_password {}
# Fetching VPC or SubnetIDS
data "aws_vpcs" "eks-vpc" {
  tags = {
    service =  var.service
  }
}
data "aws_subnet_ids" "subnetids" {
  vpc_id = element(tolist(data.aws_vpcs.eks-vpc.ids), 0)
}

# Security Group for RDS
resource "aws_security_group" "rdssg" {
  name        = "${var.rds_name}db"
  description = "security group for webservers"
  vpc_id      = element(tolist(data.aws_vpcs.eks-vpc.ids), 0)

  # Allowing traffic only for MySQL and that too from same VPC only.
  ingress {
    description = "MYSQL"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["172.168.0.0/16"]
  }

  # Allowing all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name    = "rds sg"
    service = var.service
  }
}


# DB subnet group 
resource "aws_db_subnet_group" "dbsubnet" {
  name       = "${var.rds_name}_main"
  subnet_ids = tolist(data.aws_subnet_ids.subnetids.ids)
  tags = {
    Name    = "My DB subnet group"
    service = var.service
  }
}


# Creating DB instance 
resource "aws_db_instance" "mydb" {
  depends_on        = [aws_security_group.rdssg, aws_db_subnet_group.dbsubnet]
  allocated_storage = 20
  storage_type      = "gp2"
  # Using MYSQL engine for DB
  engine = "mysql"
  # Defining the Security Group Created
  vpc_security_group_ids = [aws_security_group.rdssg.id]
  engine_version         = "5.7.30"
  instance_class         = "db.t2.micro"
  # DB security group name to specify the VPC
  db_subnet_group_name = aws_db_subnet_group.dbsubnet.name
  # Giving Credentials
  name                 = var.mysql_database
  username             = var.mysql_username
  password             = var.mysql_password
  parameter_group_name = "default.mysql5.7"
  # Making the RDS/ DB publicly accessible so that end point can be used
  publicly_accessible = true
  # Setting this true so that there will be no problem while destroying the Infrastructure as it won't create snapshot
  skip_final_snapshot = true
  identifier = var.rds_name

  tags = {
    Name    = var.rds_name
    service = var.service
  }
}