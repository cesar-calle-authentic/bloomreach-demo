resource "null_resource" "build" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = <<-EOT
        cd ..
        mvn $MAVEN_CLI_OPTS clean verify
        mvn $MAVEN_CLI_OPTS install
        mvn $MAVEN_CLI_OPTS -Pdocker.build
        export DOCKER_REGISTRY=$AWS_DOCKER_REGISTRY
        aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
        export VERSION=$( mvn help:evaluate -Dexpression=project.version -q -DforceStdout )
        export ARTIFACT_ID=$( mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout )
        export GROUP_ID=$( mvn help:evaluate -Dexpression=project.groupId -q -DforceStdout )
        docker tag $GROUP_ID/$ARTIFACT_ID:$VERSION $DOCKER_REGISTRY:$VERSION
        docker tag $GROUP_ID/$ARTIFACT_ID:$VERSION $DOCKER_REGISTRY:latest
        docker push $DOCKER_REGISTRY:$VERSION
        docker push $DOCKER_REGISTRY:latest 
    EOT
  }
}





